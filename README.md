# Hexdump

Collection of **file** dump tool, **bytes** character string

**Hexdump** can be used in **forensic** science when **searching** data in files. For example, we can recover **urls**, **passwords** in **clear** or **encrypted** form, **libraries** used by the program, **character strings**.

**Hexdump** is often accompanied with the programs **"strings"**, **"ltrace"**, **"ptrace"**, **"strace"** under **Linux**.

## Documentation
* https://en.wikipedia.org/wiki/Hex_dump

* List of **file signatures** 
    *  https://en.wikipedia.org/wiki/List_of_file_signatures
    *  https://www.filesignatures.net/index.php?page=all

## Dependencies

* Python 3 (https://www.python.org/downloads/release/python-372/)

## Terminal
<pre>
    # Displays all.
    Hexdump.py -p Test.exe
    
    # Does not display "........................"
    Hexdump.py -p Test.exe -o
    
    # Change block.
    Hexdump.py -p Test.exe -b 16
    Hexdump.py -p Test.exe -b 16 -o
    
    # Stores the result in a file.
    Hexdump.py -p Test.exe > data.txt
    Hexdump.py -p Test.exe -o > data.txt
    Hexdump.py -p Test.exe -b 16 > data.txt
    Hexdump.py -p Test.exe -b 16 -o > data.txt
    
    # Represents the character string in hexadecimal.
    Hexdump.py -s "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices."
    Hexdump.py -s 'Hello world !' -o
</pre>

### **Windows**
<pre>
    # First way.
    Hexdump -p Test.exe
    
    # Other way
    python Hexdump -p Test.exe
    
    # Powershell 
    .\Hexdump -p Test.exe
</pre>
    
### **Linux**
<pre>
    # Retrieves file signatures, most often at Offset 0.
    ./Hexdump.py -p Test.exe > data.txt | head -n 1
    ./Hexdump.py -p Test.exe -o > data.txt | head -n 1
    ./Hexdump.py -p Test.exe -b 16 > data.txt | head -n 1
    ./Hexdump.py -p Test.exe -b 16 -o > data.txt | head -n 1
</pre>

### **Mac**
<pre>
    # Retrieves file signatures, most often at Offset 0
    python3 Hexdump.py -p Test.exe > data.txt | head -n 1
    python3 Hexdump.py -p Test.exe -o > data.txt | head -n 1
    python3 Hexdump.py -p Test.exe -b 16 > data.txt | head -n 1
    python3 Hexdump.py -p Test.exe -b 16 -o > data.txt | head -n 1
</pre>

## Script

**Linux** : #!/usr/bin/python3

```python
    #!/usr/bin/python3

    from Hexdump import Hexdump
    
    #Hexdump.file2dump("Test.exe", optimize=True)
    #Hexdump.file2dump("Test.exe", 16, True)
    
    Hexdump.byte2dump(b's\x00c\x00r\x00i\x00p\x00t\x00.\x00p\x00y\x00\x00\x00l\x002\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00script (2).lnk\x00\x00N\x00\t\x00\x04\x00\xef\xbe\x00\x00\x00\x00\x00\x00\x00\x00.\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00s\x00c\x00r\x00i\x00p\x00t\x00 \x00(\x002\x00)\x00.\x00l\x00n\x00k\x00\x00\x00\x1e\x00\x00\x00')
```

### **Windows**
<pre>
    # To be executed in a terminal (CMD).
    script.py
    
    # Another way to execute the script in a terminal.
    python script.py
    
    # Powershell
    .\script.py
    
    # Double click on the "script.py" file provided that you put an instruction that allows you to pause at the end of the "script.py" file.
</pre>
  
### **Linux**

<pre>
    chmod +x script.py
    ./script.py
</pre>

### **Mac**
<pre>
    python3 script.py
</pre>

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/