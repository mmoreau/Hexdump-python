# Changelog

## v0.2.1 (21/02/2019)
* **Change** of **function** name: **Hexdump.file()** becomes **Hexdump.file2dump()**
* **Change** of **function** name: **Hexdump.byte()** becomes **Hexdump.byte2dump()**
* **Reorganization** of the **source code** of the **Hexdump class**

## v0.2.0 (12/12/2018)

* **Rename** the function "**Hexdump**" to "**file**" of the class "Hexdump.py" 
    * Hexdump.file(**path**, **optimize**=False)
        * **path** : The path of the file 
        * **optimize** : If the display needs to be optimized so as not to add repetitive "......................"
* **Added** a feature named "**byte**" of the class "Hexdump.py" 
    * Hexdump.byte(**String**, **optimize**=False)
        * **String** : Bytes type character string
        * **optimize** : If the display needs to be optimized so as not to add repetitive "......................"
* **Some code improvements** in the "Hexdump.py" class